﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace calc1
{
    public partial class Form1 : Form
    {
        Double resultValue = 0; 
        String operationPerfomed = "";
        bool isOperationPerfomed = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button_click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (isOperationPerfomed)) 
            { 
                textBox1.Clear(); 
            }
            isOperationPerfomed = false;
            Button button = (Button)sender;
            textBox1.Text = textBox1.Text + button.Text;
        }

        private void operator_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (resultValue != 0) 
            {
                button20.PerformClick();
                operationPerfomed = button.Text;
                isOperationPerfomed = true;
            }
            operationPerfomed = button.Text;
            resultValue = Double.Parse(textBox1.Text);
            isOperationPerfomed = true;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
        }

        private void button19_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            resultValue = 0;
        }
       

        private void button20_Click(object sender, EventArgs e)
        {
            switch (operationPerfomed)
            {
                case "+":
                    textBox1.Text = (resultValue + Double.Parse(textBox1.Text)).ToString();
                    break;
                case "-":
                    textBox1.Text = (resultValue - Double.Parse(textBox1.Text)).ToString();
                    break;
                case "*":
                    textBox1.Text = (resultValue * Double.Parse(textBox1.Text)).ToString();
                    break;
                case "/":
                    textBox1.Text = (resultValue / Double.Parse(textBox1.Text)).ToString();
                    break;
                case "1/x":
                    textBox1.Text = (1 / Double.Parse(textBox1.Text)).ToString();
                    break;
                case "sqr":
                    textBox1.Text = (Double.Parse(textBox1.Text) * Double.Parse(textBox1.Text)).ToString();
                    break;
                default:
                    break;

            }
        }

       




    }
}
